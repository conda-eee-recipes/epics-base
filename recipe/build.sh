#!/bin/bash

# Compile epics-base
make -j$(getconf _NPROCESSORS_ONLN)

# Create files to set/unset variables when running
# activate/deactivate
EPICS_BASES_PATH="$CONDA_PREFIX/bases"
BASE=3.15.4
EPICS_BASE="${EPICS_BASES_PATH}/base-${BASE}"
EPICS_HOST_ARCH="$(startup/EpicsHostArch)"
EPICS_BASE_BIN="$EPICS_BASE/bin/$EPICS_HOST_ARCH"
EPICS_MODULES_PATH="$CONDA_PREFIX/modules"

# EPICS_BASE is created by make but we need to create EPICS_MODULES_PATH
mkdir -p ${EPICS_MODULES_PATH}

# Compiling ca-gateway requires access to private CAS header casCtx.h
# See https://github.com/epics-extensions/ca-gateway/blob/R2-1-0-0/src/Makefile#L154
## HACK: Needs access to private CAS header casCtx.h
#USR_INCLUDES += -I$(EPICS_BASE)/src/cas/generic
#USR_INCLUDES += -I$(EPICS_BASE)/src/ca/legacy/pcas/generic
# Add src/ca/legacy/pcas/generic to the epics-base package
# (src/cas doesn't exist in 3.15)
tar cf - src/ca/legacy/pcas/generic/*.h | tar -C ${EPICS_BASE} -xvf -

# Instead of creating links we should add EPICS_BASE_BIN to the PATH
# But modifying PATH in deactivate.d doesn't work in conda 4.4.11
# This should be changed when it's fixed
cd $CONDA_PREFIX/bin
for filename in caget cainfo caput camonitor caRepeater softIoc
do
  ln -s ../bases/base-${BASE}/bin/$EPICS_HOST_ARCH/$filename $filename
done

mkdir -p $CONDA_PREFIX/etc/conda/activate.d
cat <<EOF > $CONDA_PREFIX/etc/conda/activate.d/epics_vars.sh
export BASE="$BASE"
export EPICS_BASES_PATH="$EPICS_BASES_PATH"
export EPICS_MODULES_PATH="$EPICS_MODULES_PATH"
export EPICS_BASE="$EPICS_BASE"
export EPICS_HOST_ARCH="$EPICS_HOST_ARCH"
export EPICS_BASE_BIN="$EPICS_BASE_BIN"
EOF

mkdir -p $CONDA_PREFIX/etc/conda/deactivate.d
cat <<EOF > $CONDA_PREFIX/etc/conda/deactivate.d/epics_vars.sh
unset BASE
unset EPICS_BASES_PATH
unset EPICS_MODULES_PATH
unset EPICS_BASE
unset EPICS_HOST_ARCH
unset EPICS_BASE_BIN
EOF
