diff --git a/src/ioc/dbtemplate/dbLoadTemplate.h b/src/ioc/dbtemplate/dbLoadTemplate.h
index a6ca60617..a35b6fcc9 100644
--- a/src/ioc/dbtemplate/dbLoadTemplate.h
+++ b/src/ioc/dbtemplate/dbLoadTemplate.h
@@ -4,7 +4,7 @@
 * Copyright (c) 2002 The Regents of the University of California, as
 *     Operator of Los Alamos National Laboratory.
 * EPICS BASE is distributed subject to a Software License Agreement found
-* in file LICENSE that is included with this distribution. 
+* in file LICENSE that is included with this distribution.
 \*************************************************************************/
 /* dbLoadTemplate.h */
 
@@ -15,4 +15,11 @@
 epicsShareFunc int dbLoadTemplate(
     const char *sub_file, const char *cmd_collect);
 
+/* The following is for path */
+#include "ellLib.h"
+typedef struct dbPathNode {
+	ELLNODE		node;
+	char		*directory;
+} dbPathNode;
+
 #endif /*INCdbLoadTemplateh*/
diff --git a/src/ioc/dbtemplate/dbLoadTemplate.y b/src/ioc/dbtemplate/dbLoadTemplate.y
index 1a4a47caf..714f653eb 100644
--- a/src/ioc/dbtemplate/dbLoadTemplate.y
+++ b/src/ioc/dbtemplate/dbLoadTemplate.y
@@ -11,10 +11,13 @@
 #include <stdlib.h>
 #include <stddef.h>
 #include <string.h>
+#include <ctype.h>
 
 #include "osiUnistd.h"
+#include "osiFileName.h"
 #include "macLib.h"
 #include "dbmf.h"
+#include "ellLib.h"
 
 #include "epicsExport.h"
 #include "dbAccess.h"
@@ -29,6 +32,17 @@ static char **vars = NULL;
 static char *db_file_name = NULL;
 static int var_count, sub_count;
 
+/* Handle EPICS_DB_INCLUDE_PATH. The following four functions are copied from
+ * dbLexRoutines.c and dbStaticLib.c.  The only modification is that a global
+ * variable is used to hold the linked list of paths instead of the DBBASE
+ * container. */
+
+ELLLIST *ppathList = 0;
+static char *dbOpenFile(const char *filename,FILE **fp);
+static long dbAddPath(const char *path);
+static long dbAddOnePath (const char *path, unsigned length);
+static void dbFreePath();
+
 /* We allocate MAX_VAR_FACTOR chars in the sub_collect string for each
  * "variable=value," segment, and will accept at most dbTemplateMaxVars
  * template variables.  The user can adjust that variable to increase
@@ -341,7 +355,15 @@ int dbLoadTemplate(const char *sub_file, const char *cmd_collect)
         return -1;
     }
 
-    fp = fopen(sub_file, "r");
+    char *penv;
+    penv = getenv("EPICS_DB_INCLUDE_PATH");
+    if(penv){
+        dbAddPath(penv);
+    } else {
+        dbAddPath(".");
+    }
+    dbOpenFile(sub_file, &fp);
+    dbFreePath();
     if (!fp) {
         fprintf(stderr, "dbLoadTemplate: error opening sub file %s\n", sub_file);
         return -1;
@@ -390,3 +412,127 @@ int dbLoadTemplate(const char *sub_file, const char *cmd_collect)
     }
     return 0;
 }
+
+static long dbAddOnePath (const char *path, unsigned length)
+{
+    dbPathNode *pdbPathNode;
+	
+    pdbPathNode = (dbPathNode *)calloc(1, sizeof(dbPathNode));
+    pdbPathNode->directory = (char *)calloc(length+1, sizeof(char));
+    strncpy(pdbPathNode->directory, path, length);
+    pdbPathNode->directory[length] = '\0';
+    ellAdd(ppathList, &pdbPathNode->node);
+    return 0;
+}
+
+static long dbAddPath(const char *path)
+{
+    const char	*pcolon;
+    const char	*plast;
+    unsigned	expectingPath;
+    unsigned	sawMissingPath;
+	
+    if(!ppathList) {
+	ppathList = calloc(1,sizeof(ELLLIST));
+	ellInit(ppathList);
+    }
+    if (!path) return(0); /* Empty path strings are ignored */
+    /* care is taken to properly deal with white space
+     * 1) preceding and trailing white space is removed from paths
+     * 2) white space inbetween path separator counts as an empty name
+     *		(see below)
+     */
+    expectingPath = FALSE;
+    sawMissingPath = FALSE;
+    while (*path) {
+	size_t len;
+
+	/* preceding white space is removed */
+	if (isspace((int)*path)) {
+	    path++;
+	    continue;
+	}
+	pcolon = strstr (path, OSI_PATH_LIST_SEPARATOR);
+	if (pcolon==path) {
+	    sawMissingPath = TRUE;
+	    path += strlen (OSI_PATH_LIST_SEPARATOR);
+	    continue;
+	}
+	if (pcolon) {
+	    plast = pcolon - 1;
+	    expectingPath = TRUE;
+	} else {
+	    plast = strlen (path) + path - 1;
+	    expectingPath = FALSE;
+	}
+	/* trailing white space is removed */
+	while (isspace((int)*plast)) {
+		plast--;
+	}
+
+	/*
+	 * len is always nonzero because we found something that
+	 * 1) isnt white space
+	 * 2) isnt a path separator
+	 */
+	len = (plast - path) + 1;
+	if (dbAddOnePath (path, len)) return (-1);
+	path += len;
+	if (pcolon) {
+	    path += strlen(OSI_PATH_LIST_SEPARATOR);
+	}
+    }
+
+    /*
+     * an empty name at beginning, middle, or end of a path string that isnt
+     * empty means current directory
+     */
+    if (expectingPath||sawMissingPath) {
+	return dbAddOnePath (".", 1);
+    }
+    return(0);
+}
+
+static char *dbOpenFile(const char *filename,FILE **fp)
+{
+    dbPathNode	*pdbPathNode;
+    char	*fullfilename;
+
+    *fp = 0;
+    if (!filename) return 0;
+    if (!ppathList || ellCount(ppathList) == 0 ||
+        strchr(filename, '/') || strchr(filename, '\\')) {
+        *fp = fopen(filename, "r");
+        fprintf(stdout, "%s\n", filename);
+        return 0;
+    }
+    pdbPathNode = (dbPathNode *)ellFirst(ppathList);
+    while (pdbPathNode) {
+        fullfilename = malloc(strlen(pdbPathNode->directory) + 
+            strlen(filename) + 2);
+        strcpy(fullfilename, pdbPathNode->directory);
+        strcat(fullfilename, "/");
+        strcat(fullfilename, filename);
+        *fp = fopen(fullfilename, "r");
+        fprintf(stdout, "%s \n", fullfilename);
+        free((void *)fullfilename);
+        if (*fp) return pdbPathNode->directory;
+        pdbPathNode = (dbPathNode *)ellNext(&pdbPathNode->node);
+    }
+    return 0;
+}
+
+static void dbFreePath()
+{
+    dbPathNode	*pdbPathNode;
+
+    if(!ppathList) return;
+    while((pdbPathNode = (dbPathNode *)ellFirst(ppathList))) {
+	ellDelete(ppathList,&pdbPathNode->node);
+	free((void *)pdbPathNode->directory);
+	free((void *)pdbPathNode);
+    }
+    free((void *)ppathList);
+    ppathList = 0;
+    return;
+}
