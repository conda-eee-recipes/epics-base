epics-base conda recipe
=======================

Home: https://github.com/epics-base/epics-base

Package license: EPICS Open License

Recipe license: BSD 2-Clause

Summary: EPICS Base - main core of Experimental Physics and Industrial Control System
